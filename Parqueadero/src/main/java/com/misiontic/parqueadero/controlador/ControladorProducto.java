/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.controlador;

import com.misiontic.parqueadero.Modelos.Producto;
import com.misiontic.parqueadero.Servicios.ServicioProducto;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Luis Gamboa
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/producto")
public class ControladorProducto {

    @Autowired
    private ServicioProducto productoservice;

    @GetMapping("/lista")
    public List<Producto> consultarTodo() {
        return productoservice.findAll();
    }

    @PostMapping
    public Producto insertar(@RequestBody Producto productoNuevo) {
        return productoservice.insertar(productoNuevo);
    }

    @PutMapping
    public Producto actualizar(@RequestBody Producto productoModificar) {
        return productoservice.actualizar(productoModificar);
    }

    @DeleteMapping(path = "/{idProducto}")
    public String eliminar(@PathVariable Integer idProducto) {
        try {
            productoservice.eliminarProducto(idProducto);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }

    @GetMapping("/disponibilidad")
    public List<Producto> Plazadisponibles(@RequestParam double disponible) {
        try {
            return productoservice.buscarDisponbilidad(disponible);
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/buscar")
    public List<Producto> buscarPorNombre(@RequestParam String nombre) {
        try {
            return productoservice.buscarPorNombre(nombre);
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/query")
    public List<Producto> buscarPorConsulta(@RequestParam String nombre) {
        try {
            return productoservice.buscarPorConsulta(nombre);
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/between")
    public List<Producto> buscarPorConsulta(@RequestParam double precioInicial,@RequestParam double precioFinal) {
        try {
            return productoservice.buscarEntrePrecio(precioInicial, precioFinal);
        } catch (Exception e) {
            return null;
        }
    }

}
