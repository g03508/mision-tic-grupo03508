/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.controlador;

import com.misiontic.parqueadero.Modelos.Detalle;
import com.misiontic.parqueadero.Servicios.ServicioDetalle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Luis Gamboa
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/detalle")
public class ControladorDetalle {
    
    @Autowired
    private ServicioDetalle servicioDetalle;
    
    @GetMapping
    public List<Detalle> MostrarDetalles(){
        return servicioDetalle.mostrarTodo();
    }
    
    @PostMapping
    public Detalle insertar(@RequestBody Detalle detalleNuevo){
        return servicioDetalle.guardarDetalle(detalleNuevo);
    }
    @PutMapping
    public Detalle ActualizarTransaccion(@RequestBody Detalle detalleActualizar){
        return servicioDetalle.guardarDetalle(detalleActualizar);
    }
    
    @DeleteMapping(path = "/{idDetalle}")
    public String eliminarTransaccion(@PathVariable Integer idDetalle){
        try {
            servicioDetalle.eliminar(idDetalle);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
    
    @GetMapping("/buscar")
    public List<Detalle> buscarPorProducto(String producto){
        return servicioDetalle.buscarPorNombreProducto(producto);
    }
    
}
