/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.controlador;

import com.misiontic.parqueadero.Modelos.Transaccion;
import com.misiontic.parqueadero.Servicios.ServicioTransaccion;
import java.util.List;
import lombok.Delegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Luis Gamboa
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/transaccion")
public class ControladorTransaccion {
    
    @Autowired
    private ServicioTransaccion servicioTransaccion;
    
    @GetMapping 
    public List<Transaccion> consultarTransacciones(){
        return servicioTransaccion.listarTodo();
    }
    
    @PostMapping
    public Transaccion guardarTransaccion(@RequestBody Transaccion nuevaTransaccion){
        return servicioTransaccion.guardar(nuevaTransaccion);
    }
    
    @PutMapping
    public Transaccion ActualizarTransaccion(@RequestBody Transaccion transaccionActualizar){
        return servicioTransaccion.guardar(transaccionActualizar);
    }
    
    @DeleteMapping(path = "/{idTransaccion}")
    public String eliminarTransaccion(@PathVariable Integer idTransaccion){
        try {
            servicioTransaccion.eliminar(idTransaccion);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
    
}
