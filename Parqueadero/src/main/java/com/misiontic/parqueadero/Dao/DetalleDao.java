/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Dao;

import com.misiontic.parqueadero.Modelos.Detalle;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Luis Gamboa
 */
public interface DetalleDao extends JpaRepository<Detalle,Integer> {
    
    @Query(value="select d.* from detalle d inner join producto p on p.idproducto=d.iddetalle WHERE p.nombreproducto = :nombreProducto",nativeQuery = true)
    List<Detalle> searchBynombreProducto(@Param("nombreProducto") String nombreProducto);
}
