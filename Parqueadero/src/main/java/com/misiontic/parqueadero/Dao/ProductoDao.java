/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Dao;

import com.misiontic.parqueadero.Modelos.Producto;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Luis Gamboa
 */
public interface ProductoDao extends JpaRepository<Producto, Integer> {
    
    //Buscar productos que cumplan con una condicion de Igual
    List<Producto> findByvalorCompra(double disponibilidad);
    
    //Buscar productos que contengan una parte del nombre
    List<Producto> findBynombreProductoContains(String nombre);
    
    //Buscar Productos por un precio entre datos
    List<Producto> findByvalorVentaBetween(double precioInicial,double precioFinal);
    
    //Buscar Atravez de una Consulta
    @Query(value="SELECT * FROM producto p WHERE p.nombreproducto = :usuario",nativeQuery = true)
    List<Producto> searchBynombreProductoStartsWith(@Param("usuario") String usuario);
    
    
}
