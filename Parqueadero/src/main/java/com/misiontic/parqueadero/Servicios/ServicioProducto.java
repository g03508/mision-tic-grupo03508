/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Servicios;

import com.misiontic.parqueadero.Dao.ProductoDao;
import com.misiontic.parqueadero.Modelos.Producto;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Luis Gamboa
 */
@Service
public class ServicioProducto {
    
    @Autowired
    private ProductoDao productoDao ;
    
    public Producto insertar(Producto pro){
        return productoDao.save(pro);
    }
    
    public Producto actualizar(Producto pro){
        return productoDao.save(pro);
    }
    
    public List<Producto> findAll() {
        return (List<Producto>) productoDao.findAll();
    }
 
    
    public void eliminarProducto(int idProducto){
        productoDao.delete(productoDao.findById(idProducto).get());
    }
    
    public void eliminarProducto(Producto pro){
        productoDao.delete(pro);
    }
    
    public List<Producto> buscarDisponbilidad(double disponible){
        return productoDao.findByvalorCompra(disponible);
    }
    
    public List<Producto> buscarPorNombre(String nombre){
        return productoDao.findBynombreProductoContains(nombre);
    }
    
    public List<Producto> buscarPorConsulta(String nombre){
        return productoDao.searchBynombreProductoStartsWith(nombre);
    }
    
    public List<Producto> buscarEntrePrecio(double precioInicial,double precioFinal){
        return productoDao.findByvalorVentaBetween(precioInicial,precioFinal);
    }
     
    
    
}
