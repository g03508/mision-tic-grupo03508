/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Servicios;

import com.misiontic.parqueadero.Dao.TransaccionDao;
import com.misiontic.parqueadero.Modelos.Transaccion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Luis Gamboa
 */
@Service
public class ServicioTransaccion {
    
    @Autowired
    private TransaccionDao transaccionDao;
    
    public List<Transaccion> listarTodo(){
        return transaccionDao.findAll();
    }
    
    public Transaccion guardar(Transaccion nuevaTransaccion){
        return transaccionDao.save(nuevaTransaccion);
    }
    
    public Transaccion buscarPorId(int idTransaccion){
        return transaccionDao.findById(idTransaccion).get();
    }
    
    public void eliminar(int idTransaccion){
        transaccionDao.delete(transaccionDao.findById(idTransaccion).get());
    }
    
    
    
}
