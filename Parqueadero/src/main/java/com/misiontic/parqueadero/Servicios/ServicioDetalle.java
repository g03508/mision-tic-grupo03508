/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Servicios;

import com.misiontic.parqueadero.Dao.DetalleDao;
import com.misiontic.parqueadero.Modelos.Detalle;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Luis Gamboa
 */
@Service
public class ServicioDetalle {
    
    @Autowired
    private DetalleDao detalleDao;
    
    public Detalle guardarDetalle(Detalle detalleNuevo){
        return detalleDao.save(detalleNuevo);
    }
    
    public List<Detalle> mostrarTodo(){
        return detalleDao.findAll();
    }
    
     public Detalle buscarPorId(int idDetalle){
        return detalleDao.findById(idDetalle).get();
    }
    
    public void eliminar(int idDetalle){
        detalleDao.delete(detalleDao.findById(idDetalle).get());
    }
    
    public List<Detalle> buscarPorNombreProducto(@RequestParam String producto){
        return detalleDao.searchBynombreProducto(producto);
    } 
    
    
}
