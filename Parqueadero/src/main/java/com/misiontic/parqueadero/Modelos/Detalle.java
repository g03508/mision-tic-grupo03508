/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misiontic.parqueadero.Modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Luis Gamboa
 */
@Table 
@Entity(name="detalle")
public class Detalle implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="iddetalle")
    private Integer idDetalle;
    
    @ManyToOne
    @JoinColumn(name="idproducto")
    private Producto producto;
    
    @ManyToOne
    @JoinColumn(name="idtransaccion")
    private Transaccion transaccion;
    
    @Column(name="valordetalle")
    private double valorDetalle;
    
    @Column(name="cantidaddetalle")
    private double cantidadDetalle;
    
    @Column(name="totaldetalle")
    private double totalDetalle;

    public Integer getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(Integer idDetalle) {
        this.idDetalle = idDetalle;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Transaccion getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Transaccion transaccion) {
        this.transaccion = transaccion;
    }

    public double getValorDetalle() {
        return valorDetalle;
    }

    public void setValorDetalle(double valorDetalle) {
        this.valorDetalle = valorDetalle;
    }

    public double getCantidadDetalle() {
        return cantidadDetalle;
    }

    public void setCantidadDetalle(double cantidadDetalle) {
        this.cantidadDetalle = cantidadDetalle;
    }

    public double getTotalDetalle() {
        return totalDetalle;
    }

    public void setTotalDetalle(double totalDetalle) {
        this.totalDetalle = totalDetalle;
    }
    
    
}
